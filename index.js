var bodyContent = document.getElementById("content");
function newItem (
	itemName, 
	popularity, 
	rating, 
	itemDescription, 
	itemImage, 
	proprietary, 
	packageManager, 
	initSystem, 
	architectures, 
	useCases,
	rolling,
	easyInstall,
	websiteUrl
) {				
	var itemElement = document.createElement("div");				
	itemElement.className = "item "+itemName;
	
	var distroTitle = document.createElement("h2");
	var distroLink = document.createElement("a");
	distroLink.href = websiteUrl;
	distroLink.appendChild(document.createTextNode(itemName));
	distroTitle.appendChild(distroLink);	
	
	var distroImage = document.createElement("img");
	distroImage.src = itemImage;				
	
	var textContainer = document.createElement("div"); 				
	textContainer.appendChild(distroTitle);
	
	var statistics = 	document.createElement("p"); 		
	statistics.appendChild(document.createTextNode("Popularity Rank: "+popularity));
	statistics.appendChild(document.createElement("br"));
	statistics.appendChild(document.createTextNode("Rating: "+rating));
	
	textContainer.appendChild(statistics);
	textContainer.innerHTML += itemDescription;
	textContainer.className = "textContainer";
	
	itemElement.appendChild(textContainer);
	itemElement.appendChild(distroImage);
	
	var item = {
		itemName: itemName,
		element: itemElement,
		popularity: popularity,
		rating: rating,
		itemDescription: itemDescription,
		proprietary: proprietary,
		packageManager: packageManager,
		initSystem: initSystem,
		architectures: architectures,
		useCases: useCases,
		rolling: rolling,
		easyInstall: easyInstall
	};
	bodyContent.appendChild(itemElement);
	return item;
}
var items = [
	newItem(
		"Debian", 
		2, 
		8.5, 
		"Debian, known as \"The Universal Operating System\" is a highly influential Gnu/Linux distribution started in 1993. It is named after its original creator, Ian Murdoch, and his then-girlfriend Debra Lynn. The Debian project is volunteer-driven and democratically organized. It takes a strong stance on software freedom, including only free software in standard installations. The system is known for being highly stable, including only packages which have been thoroughly tested. ",
		"img/debian.svg",
		0,
		"dpkg",
		"systemd",
		["x86","x86-64","ppc-64","arm"],
		["desktop","server"],
		0,
		1,
		"https://www.debian.org"
	),
	newItem(
		"Gentoo", 
		39, 
		9.57,
		"Gentoo Linux is named after a fast-swimming species of penguin. The system is source-based, meaning that packages are compiled for a user's particular machine. This allows heavy optimization, which allows programs to run faster than they might if distributed as pre-compiled binaries. Additionally, users are able to heavily customize the software on their system through the advanced package management and \"use flags.\" Gentoo is not recommended for beginner users or those without experience compiling software. Unlike many other modern systems, Gentoo does not by default use the controversial init system, \"systemd,\" opting for a more traditional Unix init.",
		"img/gentoo.svg",
		1,
		"portage",
		"openrc",
		["x86","x86-64","ppc","ppc-64","arm"],
		["desktop","server"],
		1,
		0,
		"https://www.gentoo.org/"
	),
	newItem(
		"Ubuntu", 
		4, 
		9.51, 
		"The name \"Ubuntu\" is a Bantu word  meaning \"Humanity to others.\" This Debian-based distro was designed to make Linux accessible and easy to use for everyone. Ubuntu is frequently recommended to beginners for its ease of use and excellent level of community and commercial support. It is developed by Canonical, a company founded by the South African entrepreneur Mark Shuttleworth. Ubuntu comes in many variants and is one of the most popular distributions for Desktops, Servers, and the Internet of Things.",
		"img/ubuntu.svg",
		1,
		"dpkg",
		"systemd",
		["x86","x86-64","ppc-64","arm"],
		["desktop","server"],
		0,
		1,
		"https://www.ubuntu.com"
	),
	newItem(
		"Arch",
		 13,
		 9.47,
		 "Arch Linux is a rolling release distribution based on the KISS principle: \"Keep it Simple, Stupid.\" The standard install process includes only a minimal base system so that the user can explicitly select the software on their machine. This allows a highly customizable system, but is often a barrier to entry for beginner users. The Arch Linux Wiki is a popular and highly informative resource for both Arch and other Linux users. Arch packages remain very close to upstream with few patches or modifications. In addition to the official packages, Arch users have access to the Arch User Repository (AUR), which includes a wide array of software packaged by users.",
		 "img/arch.svg",
		 1,
		 "pacman",
		 "systemd",
		 ["x86-64","arm"],
		["desktop","server"],
		1,
		0,
		"https://www.archlinux.org/"
	),
	newItem(
		"Mint", 
		1,
		8.82, 
		"Linux Mint is a community-directed distribution based on Ubuntu. It is focused around desktop users, with elegance and ease of use as its primary goals. The Mint team develops the Cinnamon desktop environment, which employs the modern technology of the Gnome 3 desktop but which provides a more traditional user experience based on the desktop metaphor. Mint includes common software by default, including proprietary media codecs and software necessary for DVD playback. ",
		"img/mint.svg",
		1,
		"dpkg",
		 "systemd",
		 ["x86","x86-64","ppc-64","arm"],
		 ["desktop"],
		 0,
		 1,
		 "https://www.linuxmint.com/"
	),
	newItem(
		"OpenSUSE", 
		6, 
		8.78, 
		"OpenSUSE is a community project sponsored by the German company SUSE, which also produces SUSE Linux Enterprise. Opensuse releases the standard \"Leap\" version and the rolling-release \"Tumbleweed\" version. Opensuse includes many useful tools such as YaST, for graphical configuration and the Open Build Service, which allows for easy compilation of a wide array of software packages.",
		"img/opensuse.svg",
		1,
		"rpm",
		 "systemd",
		 ["x86-64"],
		["desktop","server"],
		1,
		1,
		"https://www.opensuse.org/"
	),
	newItem(
		"Slackware", 
		32, 
		9.63, 
		"Slackware has the distinction of being the oldest Linux distribution still receiving updates. It has a traditional Unix-like design, with BSD-style init and a very simple package management scheme. Unlike many modern package managers like Apt and Yum, pkgtool does not resolve dependencies automatically. Many Slackware users compile their own software using a community-curated set of scripts called SlackBuilds, allowing for a high degree of customization and performance.",
		"img/slackware.png",
		1,
		"pkgtool",
		 "bsd",
		 ["x86","x86-64","arm"],
		["desktop","server"],
		0,
		0,
		"http://www.slackware.com/"
	),
	newItem(
		"Fedora", 
		7, 
		8.87, 
		"Sponsored by Red Hat, a company highly invested in open source software, Fedora is an innovative distribution with fast release cycles. It is often an early-adopter of new technologies, such as the Wayland display protocol, Flatpack, and SELinux. Fedora uses the RPM package format and the DNF package manager. It includes only free software with exceptions for firmware necessary operate certain machines.",
		"img/fedora.svg",
		1,
		"rpm",
		 "systemd",
		 ["x86","x86-64","ppc-64","arm"],
		["desktop","server"],
		0,
		1,
		"https://getfedora.org/"
	),
	newItem(
		"Trisquel", 
		149, 
		7.31, 
		"Trisquel Gnu/Linux is a 100% free distribution based on Ubuntu. It includes modified versions of the kernel and some other software, removing all proprietary components. For this it is endorsed by the Free Software Foundation. Trisquel is named for the triskelion, a Celtic symbol which appears in its logo.",
		"img/trisquel.svg",
		0,
		"dpkg",
		 "systemd",
		 ["x86","x86-64"],
		 ["desktop"],
		 0,
		 0,
		 "https://trisquel.info/"
	),
	newItem(
		"Guixsd",
		131, 
		10, 
		"GuixSD is in beta and not considered suitable for day-to-day use. It is an official GNU Project based on the sophisticated and purely functional Guix package manager, which offers features such as  transactional upgrades and roll-backs, unprivileged package management, and per-user profiles. Guix is based around the concepts of the Nix package manager, but employs the Guile programming language, providing Guile Scheme APIs for easy hackability.",
		"img/guixsd.svg",
		0,
		"guix",
		 "shepherd",
		 ["x86","x86-64"],
		 ["experimental"],
		 0,
		 0,
		 "https://gnu.org/software/guix"
	),
	newItem(
		"Manjaro", 
		3, 
		8.8, 
		"Manjaro Linux is an Arch-derived community distribution. It offers a graphical installer and easy access to the Arch User Repository. Releases include a fully-ready desktop environment with beautiful custom theming and a curated set of common applications. Like Arch, Manjaro uses a rolling-release model, but it holds back packages for a short period so that they are better tested before reaching users.",
		"img/manjaro.svg",
		1,
		"pacman",
		 "systemd",
		 ["x86-64"],
		 ["desktop"],
		 1,
		 1,
		 "https://manjaro.org/"
	),
	newItem(
		"Antergos", 
		5, 
		8.61, 
		"Antergos is closely related to Arch Linux, using the same repositories and following its design closely. However, Antergos includes a usable desktop out-of-the-box, making it more accessible to beginners. It cooperates with designers such as the Numix project to provide a pleasing experience by default.",
		"img/antergos.png",
		1,
		"pacman",
		 "systemd",
		 ["x86-64"],
		 ["desktop"],
		 1,
		 1,
		 "https://antergos.com/"
	),
	newItem(
		"Alpine", 
		48, 
		8.83, 
		"Alpine Linux is a relatively new and very lightweight Linux distribution intended for server and embedded use. Instead of the GNU coreutils and C library found on most other distributions, Alpine uses BusyBox and Musl, allowing for a highly minimalistic system. Alpine is oriented towards security, with a hardened kernel and binaries compiled to prevent buffer overflow exploits.",
		"img/alpine.svg",
		1,
		"apk",
		 "openrc",
		 ["x86","x86-64","arm"],
		 ["server"],
		 0,
		 0,
		 "https://alpinelinux.org/"
	),
	newItem(
		"Parabola", 
		87, 
		7.67, 
		"Parabola Gnu/Linux-libre is derived from Arch Linux, but takes additional steps to be a completely free distribution. Parabola includes only free software in its repositories and ships the modified Linux-Libre kernel, which removes proprietary binary blobs. While Arch no longer supports 32 bit architectures, Parabola continues to support older machines, as many modern computers are unable to run with entirely free software. Parabola is endorsed by the Free Software Foundation as a fully-free ",
		"img/parabola.svg",
		0,
		"pacman",
		 "systemd",
		 ["x86","x86-64"],
		 ["desktop","server"],
		 1,
		 0,
		 "https://www.parabola.nu/"
	),
	newItem(
		"Mageia", 
		19, 
		8.78, 
		"Megeia is a fork of the now-defunct Mandriva distribution and is built by many of its former contributors. It is developed with the help of Mageia.org, a French-based, community-oriented, not-for-profit organization. It is governed according to the Mageia Code of Conduct and Constitution.",
		"img/mageia.png",
		1,
		"rpm",
		 "systemd",
		 ["x86","x86-64"],
		 ["desktop","server"],
		 0,
		 1,
		 "http://www.mageia.org/"
	)
]
function comparepopularity(a,b) {
	if (a.popularity > b.popularity) { return 1; }
	if (b.popularity > a.popularity) { return -1; }
	return 0;
}
function comparerating(a,b) {
	if (a.rating < b.rating) { return 1; }
	if (b.rating < a.rating) { return -1; }
	return 0;
}

var showProprietary = true;			
var showNonRolling = true;	
var showHardInstall = true;		

var noDistro = {
	itemName: "NoDistro"
}

function sortItems() {
	if (document.myform.freeCheckbox.checked == true) {
		showProprietary = false;		
	} else {
		showProprietary = true;				
	}
	if (document.myform.rollingCheckbox.checked == true) {
		showNonRolling = false;		
	} else {
		showNonRolling = true;
	}
	if (document.myform.easyCheckbox.checked == true) {
		showHardInstall = false;		
	} else {
		showHardInstall = true;
	}
	for (var i = 0; i < items.length; i++) {
		bodyContent.removeChild(items[i].element);
	}
	if (document.getElementById("sortSelect").value == "popularity") {
		items.sort(comparepopularity);
	} else if (document.getElementById("sortSelect").value == "rating") {
		items.sort(comparerating);
	}
	var lastDistro = noDistro;
	for (var i = 0; i < items.length; i++) {
		var show = true;
		items[i].element.classList.remove("hidden");
		if (!showProprietary) {
			if (items[i].proprietary == 1) {
				show = false;
			}
		}
		if (!showNonRolling) {
			if (items[i].rolling == 0) {
				show = false;
			}
		}
		if (!showHardInstall) {
			if (items[i].easyInstall == 0) {
				show = false;
			}
		}
		var selectedPackageManager = document.getElementById("packageManagerSelect").value;
		if (selectedPackageManager != "any") {
			if (selectedPackageManager != items[i].packageManager) {
				show = false;
			}
		}
		var selectedInitSystem = document.getElementById("initSystemSelect").value;
		if (selectedInitSystem != "any") {
			if (selectedInitSystem != items[i].initSystem) {
				show = false;
			}
		}
		var selectedArchitecture = document.getElementById("architectureSelect").value;
		if (!(items[i].architectures.indexOf(selectedArchitecture) >= 0)) {
			items[i].element.classList.add("hidden");
			show = false;
		}
		var selectedUseCase = document.getElementById("useSelect").value;
		if (selectedUseCase != "any") {
			if (!(items[i].useCases.indexOf(selectedUseCase) >= 0)) {
				
				show = false;
			}
		}
		if (show) {
			lastDistro = items[i];
		} else {
			items[i].element.classList.add("hidden");
		}
		bodyContent.appendChild(items[i].element);
		
		document.getElementById("content").className = (lastDistro.itemName);
	}
	if (document.getElementById("content").className == "NoDistro"){
		document.getElementById("emptySetMessage").className = "";
	} else {
		document.getElementById("emptySetMessage").className = "hidden";
	}
}

function resetSort(){
	document.getElementById("packageManagerSelect").value = "any";
	document.getElementById("initSystemSelect").value = "any";
	document.getElementById("architectureSelect").value = "x86-64";
	document.getElementById("useSelect").value = "any";
	document.getElementById("sortSelect").value = "popularity";
	document.myform.easyCheckbox.checked = false;
	document.myform.rollingCheckbox.checked = false;
	document.myform.freeCheckbox.checked = false;
	sortItems();
}

sortItems();
